mod utils;

fn main() {
    utils::start(None);
    loop {
        utils::prompt();
        let mut commands = String::new();
        std::io::stdin().read_line(&mut commands).unwrap();
        utils::command(&commands);
    }
}
