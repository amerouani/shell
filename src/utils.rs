use hostname;
use users::get_current_username;

pub fn start(motd: Option<&str>) {
    self::motd(motd);
}

pub fn motd(x: Option<&str>) {
    println!("This programs is currently in developpement, should not be accounted for a viable terminal replacement.");
    println!("It obviously comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.");
    println!("MOTD : {}", x.unwrap_or("No motd"));
    println!(
        "
     ████████╗███████╗██████╗ ███╗   ███╗███████╗██╗  ██╗███████╗██╗     ██╗     
     ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║██╔════╝██║  ██║██╔════╝██║     ██║     
        ██║   █████╗  ██████╔╝██╔████╔██║███████╗███████║█████╗  ██║     ██║     
        ██║   ██╔══╝  ██╔══██╗██║╚██╔╝██║╚════██║██╔══██║██╔══╝  ██║     ██║     
        ██║   ███████╗██║  ██║██║ ╚═╝ ██║███████║██║  ██║███████╗███████╗███████╗+
        ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝"
    );
    println!("");
}

pub fn prompt() {
    print!("{}", get_current_username().unwrap().to_str().unwrap());
    print!("@");
    print!("{}", hostname::get().unwrap().to_str().unwrap());
    print!(":$ ");
    std::io::Write::flush(&mut std::io::stdout()).expect("flushing error");
}

pub fn command(cmd: &str) {
    let tokens = self::analyze::lexer(cmd);

    match tokens {
        Ok(value) => {
            self::analyze::parser(&value);
        }
        Err(value) => {
            eprintln!("An error has occured : {}", value);
        }
    }
}

pub mod analyze {

    use std::process::{ChildStdout, Command, Stdio};

    #[derive(Debug, PartialEq, Eq)]
    pub enum Token {
        PIPE,
        AND,
        RBRACKET,
        LBRACKET,
        IDENTIFIER(String),
    }

    pub fn parser(x: &Vec<Token>) {
        let mut process: Vec<Command> = Vec::new();

        let mut waiting_args = false;
        let mut waiting_pipe = false;
        let mut pipe: Option<ChildStdout> = None;

        for (index, element) in x.iter().enumerate() {
            if index + 1 == x.len() {
                if let Token::IDENTIFIER(value) = element {
                    if waiting_args && waiting_pipe {
                        let mut current = process.pop().expect("can't open the process table.");
                        current.arg(value);
                        current
                            .stdin(pipe.take().unwrap())
                            .env("PATH", "/bin/")
                            .spawn()
                            .expect("Failed to start pipe process")
                            .wait()
                            .expect("failed to start process");
                    } else if waiting_args {
                        let mut current = process.pop().expect("can't open the process table.");
                        current
                            .arg(value)
                            .env("PATH", "/bin/")
                            .spawn()
                            .unwrap()
                            .wait()
                            .expect("failed to start process");
                    } else {
                        Command::new(value)
                            .env("PATH", "/bin/")
                            .spawn()
                            .unwrap()
                            .wait()
                            .expect("failed to start process");
                    }
                } else {
                    eprintln!("Operators must be surrounded by identifier.");
                }
            } else {
                match element {
                    Token::IDENTIFIER(value) => {
                        if waiting_args {
                            let mut current = process.pop().expect("can't open the process table.");
                            current.arg(value);
                            process.push(current);
                        } else {
                            process.push(Command::new(value));
                            waiting_args = true;
                        }
                    }
                    Token::PIPE => {
                        waiting_args = false;
                        waiting_pipe = true;
                        pipe = process
                            .pop()
                            .unwrap()
                            .stdout(Stdio::piped())
                            .spawn()
                            .unwrap()
                            .stdout;
                    }
                    Token::AND => {
                        waiting_args = false;
                        waiting_pipe = false;
                        process
                            .pop()
                            .unwrap()
                            .spawn()
                            .unwrap()
                            .wait()
                            .expect("failed to start process");
                    }
                    _ => {}
                }
            }
        }
    }

    pub fn lexer(x: &str) -> Result<Vec<Token>, String> {
        let mut tokenlst: Vec<Token> = Vec::new();

        let characters: Vec<char> = x.chars().collect();

        let mut it = x.chars().enumerate();

        while let Some((index, elem)) = it.next() {
            if is_ident(elem) {
                let to_skip = dfa_ident(&characters[index..], &mut tokenlst);
                it.nth(to_skip - 1);
            } else if elem == '|' {
                tokenlst.push(Token::PIPE);
            } else if elem == ' ' {
                continue;
            } else if elem == '\n' {
                break;
            } else if elem == '>' {
                tokenlst.push(Token::RBRACKET);
            } else if elem == '<' {
                tokenlst.push(Token::LBRACKET);
            } else if elem == '&' {
                let to_skip = dfa_and(&characters[(index + 1)..], &mut tokenlst);
                if to_skip == 0 {
                    return Err(format!(
                        "Unfinished syntax. '&'. Maybe you tried to use '&&'. "
                    ));
                }
                it.nth(to_skip - 1);
            } else {
                return Err(format!("character \"{}\" not supported", elem));
            }
        }

        Ok(tokenlst)
    }

    fn dfa_ident(x: &[char], y: &mut Vec<Token>) -> usize {
        let mut value = String::new();
        let mut i: usize = 0;
        for elem in x {
            if is_ident(*elem) {
                value.push(*elem);
                i = i + 1;
            } else {
                break;
            }
        }
        y.push(Token::IDENTIFIER(value));
        i
    }

    fn dfa_and(x: &[char], y: &mut Vec<Token>) -> usize {
        let mut i: usize = 0;
        if x[0] == '&' {
            y.push(Token::AND);
            i = 1;
        }

        i
    }

    fn is_ident(x: char) -> bool {
        match x {
            '\\' => true,
            '/' => true,
            '.' => true,
            '-' => true,
            '"' => true,
            '\'' => true,
            'A'..='Z' => true,
            'a'..='z' => true,
            '0'..='9' => true,
            _ => false,
        }
    }
}
